/**
 * 初始化商品信息详情对话框
 */
var ItemInfoInfoDlg = {
    itemInfoInfoData : {}
};

/**
 * 清除数据
 */
ItemInfoInfoDlg.clearData = function() {
    this.itemInfoInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemInfoInfoDlg.set = function(key, val) {
    this.itemInfoInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ItemInfoInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ItemInfoInfoDlg.close = function() {
    parent.layer.close(window.parent.ItemInfo.layerIndex);
}

/**
 * 收集数据
 */
ItemInfoInfoDlg.collectData = function() {
    this
    .set('id')
    .set('createTime')
    .set('modifyTime')
    .set('itemNo')
    .set('itemSubNo')
    .set('itemName');
}

/**
 * 提交添加
 */
ItemInfoInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/itemInfo/add", function(data){
        Feng.success("添加成功!");
        window.parent.ItemInfo.table.refresh();
        ItemInfoInfoDlg.close();
    },function(data){
    	alert(JSON.stringify(data));
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemInfoInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ItemInfoInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/itemInfo/update", function(data){
        Feng.success("修改成功!");
        window.parent.ItemInfo.table.refresh();
        ItemInfoInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.itemInfoInfoData);
    ajax.start();
}

$(function() {

});
