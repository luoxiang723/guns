/**
 * 商品信息管理初始化
 */
var ItemInfo = {
    id: "ItemInfoTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ItemInfo.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '修改时间', field: 'modifyTime', visible: true, align: 'center', valign: 'middle'},
            {title: '自编码', field: 'itemNo', visible: true, align: 'center', valign: 'middle'},
            {title: '条码', field: 'itemSubNo', visible: true, align: 'center', valign: 'middle'},
            {title: '商品名称', field: 'itemName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ItemInfo.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ItemInfo.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品信息
 */
ItemInfo.openAddItemInfo = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/itemInfo/itemInfo_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品信息详情
 */
ItemInfo.openItemInfoDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/itemInfo/itemInfo_update/' + ItemInfo.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品信息
 */
ItemInfo.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/itemInfo/delete", function (data) {
            Feng.success("删除成功!");
            ItemInfo.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("itemInfoId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商品信息列表
 */
ItemInfo.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    ItemInfo.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = ItemInfo.initColumn();
    var table = new BSTable(ItemInfo.id, "/itemInfo/list", defaultColunms);
    table.setPaginationType("client");
    ItemInfo.table = table.init();
});
