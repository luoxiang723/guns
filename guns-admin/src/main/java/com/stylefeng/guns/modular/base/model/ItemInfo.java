package com.stylefeng.guns.modular.base.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品信息表
 * </p>
 *
 * @author luoxiang
 * @since 2018-07-11
 */
@TableName("base_item_info")
public class ItemInfo extends Model<ItemInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField("modify_time")
    private Date modifyTime;
    /**
     * 自编码
     */
    @TableField("item_no")
    private String itemNo;
    /**
     * 条码
     */
    @TableField("item_sub_no")
    private String itemSubNo;
    /**
     * 商品名称
     */
    @TableField("item_name")
    private String itemName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemSubNo() {
        return itemSubNo;
    }

    public void setItemSubNo(String itemSubNo) {
        this.itemSubNo = itemSubNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ItemInfo{" +
        "id=" + id +
        ", createTime=" + createTime +
        ", modifyTime=" + modifyTime +
        ", itemNo=" + itemNo +
        ", itemSubNo=" + itemSubNo +
        ", itemName=" + itemName +
        "}";
    }
}
