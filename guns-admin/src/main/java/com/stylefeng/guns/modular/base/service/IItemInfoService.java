package com.stylefeng.guns.modular.base.service;

import com.stylefeng.guns.modular.base.model.ItemInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品信息表 服务类
 * </p>
 *
 * @author luoxiang
 * @since 2018-07-11
 */
public interface IItemInfoService extends IService<ItemInfo> {

}
