package com.stylefeng.guns.modular.base.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.base.dao.ItemInfoMapper;
import com.stylefeng.guns.modular.base.model.ItemInfo;
import com.stylefeng.guns.modular.base.service.IItemInfoService;

/**
 * 商品信息控制器
 *
 * @author fengshuonan
 * @Date 2018-07-11 14:33:48
 */
@Controller
@RequestMapping("/itemInfo")
public class ItemInfoController extends BaseController {

    private String PREFIX = "/base/itemInfo/";

    @Autowired
    private IItemInfoService itemInfoService;
    
    @Autowired
    private ItemInfoMapper itemInfoMapper;

    /**
     * 跳转到商品信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "itemInfo.html";
    }

    /**
     * 跳转到添加商品信息
     */
    @RequestMapping("/itemInfo_add")
    public String itemInfoAdd() {
        return PREFIX + "itemInfo_add.html";
    }

    /**
     * 跳转到修改商品信息
     */
    @RequestMapping("/itemInfo_update/{itemInfoId}")
    public String itemInfoUpdate(@PathVariable Integer itemInfoId, Model model) {
        ItemInfo itemInfo = itemInfoService.selectById(itemInfoId);
        model.addAttribute("item",itemInfo);
        LogObjectHolder.me().set(itemInfo);
        return PREFIX + "itemInfo_edit.html";
    }

    /**
     * 获取商品信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
    	if(ToolUtil.isNotEmpty(condition)){
//    		EntityWrapper<ItemInfo> itemInfoWrapper = new EntityWrapper<>();
//    		Wrapper<ItemInfo> wrapper = itemInfoWrapper.like(column, value);
    	}
        return itemInfoService.selectList(null);
    }

    /**
     * 新增商品信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ItemInfo itemInfo) {
    	if(itemInfo!=null){
    		Date now = new Date();
    		itemInfo.setCreateTime(now);
    		itemInfo.setModifyTime(now);
    		itemInfoService.insert(itemInfo);
    		
    	}
        
        return SUCCESS_TIP;
    }

    /**
     * 删除商品信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer itemInfoId) {
        itemInfoService.deleteById(itemInfoId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ItemInfo itemInfo) {
    	if(itemInfo!=null){
    		Date now = new Date();
    		if(itemInfo.getCreateTime()==null){
    			itemInfo.setCreateTime(now);
    		}
    		itemInfo.setModifyTime(now);
            itemInfoService.updateById(itemInfo);
    	}
        return SUCCESS_TIP;
    }

    /**
     * 商品信息详情
     */
    @RequestMapping(value = "/detail/{itemInfoId}")
    @ResponseBody
    public Object detail(@PathVariable("itemInfoId") Integer itemInfoId) {
        return itemInfoService.selectById(itemInfoId);
    }
}
