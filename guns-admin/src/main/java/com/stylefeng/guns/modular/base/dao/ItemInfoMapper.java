package com.stylefeng.guns.modular.base.dao;

import com.stylefeng.guns.modular.base.model.ItemInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品信息表 Mapper 接口
 * </p>
 *
 * @author luoxiang
 * @since 2018-07-11
 */
public interface ItemInfoMapper extends BaseMapper<ItemInfo> {

}
