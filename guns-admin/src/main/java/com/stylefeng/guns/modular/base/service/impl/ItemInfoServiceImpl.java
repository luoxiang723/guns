package com.stylefeng.guns.modular.base.service.impl;

import com.stylefeng.guns.modular.base.model.ItemInfo;
import com.stylefeng.guns.modular.base.dao.ItemInfoMapper;
import com.stylefeng.guns.modular.base.service.IItemInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品信息表 服务实现类
 * </p>
 *
 * @author luoxiang
 * @since 2018-07-11
 */
@Service
public class ItemInfoServiceImpl extends ServiceImpl<ItemInfoMapper, ItemInfo> implements IItemInfoService {

}
